import React, { ReactElement } from "react";

import { Grommet } from "grommet";

import CatSprite from "./components/CatSprite";

const theme = {
  global: {
    colors: {
      brand: "pink",
    },
    font: {
      family: "Roboto",
      size: "18px",
      height: "20px",
    },
  },
};

const App = (): ReactElement => {
  return (
    <>
      <Grommet theme={theme}></Grommet>
      <CatSprite
        startX={Math.random() * window.innerWidth}
        startY={Math.random() * window.innerHeight}
      />
      <CatSprite
        startX={Math.random() * window.innerWidth}
        startY={Math.random() * window.innerHeight}
      />
      <CatSprite
        startX={Math.random() * window.innerWidth}
        startY={Math.random() * window.innerHeight}
      />
      <CatSprite
        startX={Math.random() * window.innerWidth}
        startY={Math.random() * window.innerHeight}
      />
      <CatSprite
        startX={Math.random() * window.innerWidth}
        startY={Math.random() * window.innerHeight}
      />
      <CatSprite
        startX={Math.random() * window.innerWidth}
        startY={Math.random() * window.innerHeight}
      />
    </>
  );
};

export default App;
