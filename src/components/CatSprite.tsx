import React, {
  ReactElement,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react";

import styled from "styled-components";
import { throttle } from "lodash";

import sheet from "../img/catSpriteSheet.png";

interface SpriteState {
  startIndex: number;
  length: number;
}

interface Point {
  x: number;
  y: number;
}

const SPRITE_SIZE = 32;
const CAT_FPS = 13;
const SPRITE_ANIM_STATES: Record<string, SpriteState> = {
  idleOne: {
    startIndex: 0,
    length: 4,
  },
  idleTwo: {
    startIndex: 1,
    length: 4,
  },
  cleanOne: {
    startIndex: 2,
    length: 4,
  },
  cleanTwo: {
    startIndex: 3,
    length: 4,
  },
  moveOne: {
    startIndex: 4,
    length: 8,
  },
  moveTwo: {
    startIndex: 5,
    length: 8,
  },
  sleep: {
    startIndex: 6,
    length: 4,
  },
  paw: {
    startIndex: 7,
    length: 6,
  },
  jump: {
    startIndex: 8,
    length: 7,
  },
  scared: {
    startIndex: 9,
    length: 8,
  },
};

const SPRITE_ANIM_NAMES = Object.keys(SPRITE_ANIM_STATES).filter(
  (name) => name !== "moveOne" && name !== "moveTwo"
);

const useAnimationFrame = (
  fn: VoidFunction,
  fps: number
): { start: VoidFunction; stop: VoidFunction } => {
  const delta = useRef(0);
  const id = useRef(0);

  const start = useCallback((): void => {
    id.current = requestAnimationFrame((newDelta) => {
      start();
      if (newDelta - delta.current > 1000 / fps) {
        fn();
        delta.current = newDelta;
      }
    });
  }, [fn, fps]);

  const stop = useCallback((): void => cancelAnimationFrame(id.current), []);

  return {
    start,
    stop,
  };
};

const setAnimAfterDelay = (
  animationCallback: () => void,
  delay: number
): void => {
  setTimeout(animationCallback, delay);
};

const CatSprite = ({
  startX = 0,
  startY = 0,
}: {
  startX?: number;
  startY?: number;
}): ReactElement => {
  const divRef = useRef<HTMLDivElement>(null);
  const currentStep = useRef(0);
  const randomIdleTime = useRef(500 + Math.random() * 7000);

  const [currentAnim, setCurrentAnim] = useState<SpriteState>(
    SPRITE_ANIM_STATES["idleOne"]
  );
  const [mousePos, setMousePos] = useState<Point>({
    x: startX,
    y: startY,
  });

  const [faceDir, setFaceDir] = useState<number>(1);

  const [spriteSheetPos, setSpriteSheetPos] = useState<Point>({
    x: 0,
    y: 0,
  });

  const divPos = divRef.current?.getBoundingClientRect() || { x: 0, y: 0 };
  const catTransitionSpeed =
    Math.sqrt(
      Math.pow(divPos.x - mousePos.x, 2) + Math.pow(divPos.y - mousePos.y, 2)
    ) * 0.007;

  // using memo because useCallback needs inline func
  const updateMousePosition = useMemo(
    () =>
      throttle(
        ({ clientX = 0, clientY = 0 }: Partial<MouseEvent>) => {
          setFaceDir(clientX > divPos.x ? 1 : -1);
          setMousePos({
            x:
              clientX -
              SPRITE_SIZE +
              (Math.random() > 0.5 ? Math.random() * 50 : Math.random() * -50),
            y:
              clientY -
              SPRITE_SIZE +
              (Math.random() > 0.5 ? Math.random() * 50 : Math.random() * -50),
          });
          setCurrentAnim(SPRITE_ANIM_STATES["moveOne"]);
        },
        randomIdleTime.current,
        {
          leading: false,
        }
      ),
    [divPos.x]
  );

  // using memo because useCallback needs inline func
  const updateMouseAnim = useMemo(
    () =>
      throttle(
        () => {
          setCurrentAnim(SPRITE_ANIM_STATES["paw"]);
          setAnimAfterDelay(
            () => setCurrentAnim(SPRITE_ANIM_STATES["idleTwo"]),
            700
          );
        },
        700,
        {
          leading: true,
        }
      ),
    []
  );

  const update = useCallback((): void => {
    setSpriteSheetPos({
      x: currentStep.current * SPRITE_SIZE,
      y: currentAnim.startIndex * SPRITE_SIZE,
    });
    currentStep.current = (currentStep.current + 1) % currentAnim.length;
  }, [currentAnim]);

  const { start, stop } = useAnimationFrame(update, CAT_FPS);

  useEffect(() => {
    start();
    return stop;
  }, [start, stop]);

  useEffect(() => {
    const handleTouch = ({ touches }: TouchEvent): void => {
      console.log("sdadsa");
      const { clientX, clientY } = touches.item(0) || {
        clientX: 0,
        clientY: 0,
      };
      updateMousePosition({ clientX, clientY });
    };
    window.addEventListener("touchstart", handleTouch);
    window.addEventListener("mousemove", updateMousePosition);
    return () => {
      window.removeEventListener("mousemove", updateMousePosition);
      window.removeEventListener("touchstart", handleTouch);
    };
  }, [updateMousePosition]);

  useEffect(() => {
    window.addEventListener("dblclick", updateMouseAnim);
    return () => window.removeEventListener("dblclick", updateMouseAnim);
  }, [updateMouseAnim]);

  return (
    <StyledSprite
      ref={divRef}
      onTransitionEnd={() => {
        setCurrentAnim(
          SPRITE_ANIM_STATES[
            SPRITE_ANIM_NAMES[
              Math.floor(Math.random() * SPRITE_ANIM_NAMES.length)
            ]
          ]
        );
      }}
      style={{
        transition: `transform ${catTransitionSpeed}s ease`,
        transform: `translate(${mousePos.x}px, ${mousePos.y}px) scale(2.5) scaleX(${faceDir})`,
        backgroundPosition: `-${spriteSheetPos.x}px -${spriteSheetPos.y}px`,
      }}
    />
  );
};

export default CatSprite;

const StyledSprite = styled.div`
  position: absolute;
  background-image: url(${sheet});
  background-repeat: no-repeat;
  width: ${SPRITE_SIZE}px;
  height: ${SPRITE_SIZE}px;
`;
